#include <stdio.h>
#include <stdlib.h>
#include <math.h>


// Tablica przechowywująca indeksy macierzy i wektora
int ix[4] = {0, 1, 2, 3};

//Zamienia kolejnosc liczb
void zamien(int *a, int *b) {
    int tmp;
    tmp = *a;
    *a = *b;
    *b = tmp;
}

//Wykonuje dekompozycje LU macierzy. Macierze wynikowe LU są przechowywane w macierzy wejściowej.
void dekompozycja_LU(double macierz[4][4]) {
    int i, j, k, imax;
    for(i = 0; i < 4; i++) {
        // Częściowy wybór elementu podstawowego
        imax = i;
        for(j = i; j < 4; j++) {
           if(fabs(macierz[ix[j]][i]) > fabs(macierz[ix[imax]][i]))
               imax = j;
        }
        zamien(&ix[i], &ix[imax]);

        // Wykonanie jednej iteracji eliminacji Gaussa z zachowaniem
        // współczynników stanowiących macierz L
        // Wierszem bazowym jest wiersz i-ty.
        for(j = i + 1; j < 4; j++) {
            // Wyznaczenie współczynnika
            double wspolczynnik = macierz[ix[j]][i] / macierz[ix[i]][i];

            // Odjęcie wiersza bazowego pomnożonego przez współczynnik
            // od bieżącego wiersza
            for(k = i; k < 4; k++) {
                macierz[ix[j]][k] -= wspolczynnik * macierz[ix[i]][k];
            }

            // Zachowanie współczynnika w macierzy wejściowej
            macierz[ix[j]][i] = wspolczynnik;
        }
    }
}

//Rozwiązuje układ równań za pomocą podstawienia w przód oraz wstecz wykorzystując macierz po dekompozycji LU.
void policz_uklad(double macierz[4][4], double wektor[4]) {
    int i, j;

    // Obliczenie Lz = b względem z podstawieniem w przód
    for(i = 0; i < 4; i++) {
        double suma = 0.0;
        for(j = 0; j < i; j++) {
            suma += macierz[ix[i]][j] * wektor[ix[j]];
        }
        wektor[ix[i]] -= suma;
    }

    // Obliczenie Ux = z względem x podstawieniem wstecz
    for(i = 3; i >= 0; i--) {
        double suma = 0.0;
        for(j = 3; j > i; j--) {
            suma += macierz[ix[i]][j] * wektor[ix[j]];
        }
        wektor[ix[i]] = (wektor[ix[i]] - suma) / macierz[ix[i]][i];
    }
}

//Wypisuje macierz na standardowe wyjście.
void wypisz_macierz(double macierz[4][4]) {
    int i, j;
    for(i = 0; i < 4; i++) {
        for(j = 0; j < 4; j++) {
            printf("%10.6lf ", macierz[ix[i]][j]);
        }
        printf("\n");
    }
}

//Wypisuje wektor na standardowe wyjście.
void wypisz_wektor(double wektor[4]) {
    int i;
    for(i = 0; i < 4; i++) {
        printf("%10.6lf\n", wektor[ix[i]]);
    }
}


int main() {

    double macierzA[4][4] = { 
        {1.0, 2.0, 2.0, 1.0}, 
        {2.0, 4.0, 4.0, 1.0},
        {2.0, 2.0, 2.0, 1.0},
        {1.0, 1.0, 2.0, 1.0} };

    double wektorB[4] = {1.0, 2.0, 3.0, 4.0};

    printf("Macierz A przed dekompozycją:\n");
    wypisz_macierz(macierzA);

    dekompozycja_LU(macierzA);

    printf("Macierz A po dekompozycji:\n");
    wypisz_macierz(macierzA);

    policz_uklad(macierzA, wektorB);

    printf("Rozwiązanie układu równań:\n");
    wypisz_wektor(wektorB);


    return 0;
}
